
//ON CLICK
function clickSound(event){
    let audio = document.querySelector(`audio[data-key="${event.keyCode}"]`);
    let key = document.querySelector(`li[data-key="${event.keyCode}"]`);

    if (audio) {
        audio.currentTime = 0;
        audio.play();
    }
}

//ON KEYS
function playSound(event) {
    let audio = document.querySelector(`audio[data-key="${event.keyCode}"]`);
    let key = document.querySelector(`li[data-key="${event.keyCode}"]`);
  
    if (audio) {
      audio.currentTime = 0;
      audio.play();
    }
    if (key) {
        key.style.borderTop = "1px solid #777";
        key.style.borderLeft = "1px solid #999";
        key.style.borderBottom = "1px solid #999";
        key.style.boxShadow = "2px 0 3px rgba(0,0,0,0.1) inset,-5px 5px 20px rgba(0,0,0,0.2) inset,0 0 3px rgba(0,0,0,0.2)"
        key.style.background = "linear-gradient(to bottom,#fff 0%,#e9e9e9 100%)";
        key.style.transition = "0.1s ease-in"
    }
}

//END SOUND
function endSound(event) {
    let key = document.querySelector(`li[data-key="${event.keyCode}"]`);
  
    if (key) {
        key.style.borderTop = "none";
        key.style.borderLeft = "1px solid #bbb";
        key.style.borderBottom = "1px solid #bbb";
        key.style.boxShadow = "-1px 0 0 rgba(255,255,255,0.8) inset,0 0 5px #ccc inset,0 0 3px rgba(0,0,0,0.2)";
        key.style.background = "linear-gradient(to bottom,#eee 0%,#fff 100%)";
    }
}

//CALL FUNCTIONS EVENTS
window.document.addEventListener('keydown', playSound);
window.document.addEventListener('keyup', endSound);
window.document.addEventListener('click', clickSound);